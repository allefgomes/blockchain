const { GENESIS_DATA } = require("./configs/constants");

class Block {
  constructor({ timestamp, lastHash, hash, data }) {
    this.timestamp = timestamp;
    this.hash = hash;
    this.lastHash = lastHash;
    this.data = data;
  }

  static genesis() {
    return new this(GENESIS_DATA);
  }
};

module.exports = Block;
