const Block = require("../src/block");
const { GENESIS_DATA } = require("../src/configs/constants");

describe('Block', () => {
  const timestamp = '01/01/01';
  const lastHash = 'lastHash';
  const hash = 'hash';
  const data = ['blockchain', 'data'];
  const block = new Block({ timestamp, lastHash, hash, data });

  it('has a timestamp, lastHash, hash and data property', () => {
    expect(block.timestamp).toEqual(timestamp);
    expect(block.lastHash).toEqual(lastHash);
    expect(block.hash).toEqual(hash);
    expect(block.data).toEqual(data);
  });

  describe('genesis()', () => {
    const genesisBlock = Block.genesis();

    it ('returns a Block instance', () => {
      expect(genesisBlock instanceof Block).toEqual(true);
    });

    it ('return genesis data', () => { 
      expect(genesisBlock).toEqual(GENESIS_DATA);
    });
  });
});
